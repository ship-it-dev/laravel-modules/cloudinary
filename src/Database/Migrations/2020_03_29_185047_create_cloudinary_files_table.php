<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use ShipIT\Cloudinary\Models\File;

class CreateCloudinaryFilesTable extends Migration
{
    public function up()
    {
        Schema::create(File::TABLE_NAME, function (Blueprint $table) {
            $table->id();

            $table->string(File::COLUMN_PUBLIC_ID);
            $table->string(File::COLUMN_RESOURCE_TYPE);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists(File::TABLE_NAME);
    }
}
