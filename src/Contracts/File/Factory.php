<?php


namespace ShipIT\Cloudinary\Contracts\File;


use ShipIT\Cloudinary\Contracts\UploadResponse;
use ShipIT\Cloudinary\Models\File;

interface Factory
{
    public function makeFromResponse(UploadResponse $response): File;
}