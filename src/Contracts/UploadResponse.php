<?php


namespace ShipIT\Cloudinary\Contracts;


interface UploadResponse
{
    public function getSecureUrl(): string;

    public function toArray(): array;
}