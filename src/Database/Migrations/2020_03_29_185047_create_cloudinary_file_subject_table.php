<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCloudinaryFileSubjectTable extends Migration
{
    private const TABLE_NAME = 'cloudinary_file_subject';

    public function up()
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->bigInteger('file_id');
            $table->string('subject_type');
            $table->bigInteger('subject_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
