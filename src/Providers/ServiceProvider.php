<?php

namespace ShipIT\Cloudinary\Providers;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->mergeConfigFrom(__DIR__ . '/../../config/config.php', 'cloudinary');

        $this->initConfig();
    }

    private function initConfig(): void
    {
        $url = config('cloudinary.url');

        if (!$url) {
            return;
        }

        \Cloudinary::config_from_url($url);
    }

    public function register()
    {
        parent::register();

        $this->app->register(DependencyInjectionProvider::class);
        $this->app->register(EventServiceProvider::class);
    }
}
