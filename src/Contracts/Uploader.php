<?php


namespace ShipIT\Cloudinary\Contracts;


interface Uploader
{
    public function upload(
        string $path,
        UploadOptions $options
    ): UploadResponse;

    public function uploadLarge(
        string $path,
        UploadOptions $options
    ): UploadResponse;
}
