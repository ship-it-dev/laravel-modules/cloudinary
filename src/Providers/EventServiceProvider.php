<?php


namespace ShipIT\Cloudinary\Providers;


use ShipIT\Cloudinary\Models\File;
use ShipIT\Cloudinary\Observers\FileObserver;

class EventServiceProvider extends \Illuminate\Foundation\Support\Providers\EventServiceProvider
{
    public function boot()
    {
        parent::boot();

        File::observe(FileObserver::class);

    }
}
