<?php


namespace ShipIT\Cloudinary\Providers;


use ShipIT\Cloudinary\Contracts\File\Factory;
use ShipIT\Cloudinary\Contracts\Uploader as UploaderContract;
use ShipIT\Cloudinary\Contracts\UploadOptions as UploadOptionsContract;
use ShipIT\Cloudinary\Contracts\UploadResponse as UploadResponseContract;
use ShipIT\Cloudinary\Factories\FileFactory;
use ShipIT\Cloudinary\Models\Uploader;
use ShipIT\Cloudinary\Models\UploadOptions;
use ShipIT\Cloudinary\Models\UploadResponse;

class DependencyInjectionProvider extends \Illuminate\Support\ServiceProvider
{
    public function register()
    {
        parent::register();

        $this->app->singleton(
            Factory::class,
            FileFactory::class
        );

        $this->app->bind(
            UploadOptionsContract::class,
            UploadOptions::class
        );

        $this->app->bind(
            UploadResponseContract::class,
            UploadResponse::class
        );

        $this->app->singleton(
            UploaderContract::class,
            Uploader::class
        );

    }
}
