<?php


namespace ShipIT\Cloudinary\Factories;


use ShipIT\Cloudinary\Contracts\File\Factory;
use ShipIT\Cloudinary\Contracts\UploadResponse;
use ShipIT\Cloudinary\Models\File;

class FileFactory implements Factory
{

    public function makeFromResponse(UploadResponse $response): File
    {
        return new File(
            $response->toArray()
        );
    }
}