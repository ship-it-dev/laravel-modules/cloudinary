<?php


namespace ShipIT\Cloudinary\Models;


use Illuminate\Support\Fluent;
use ShipIT\Cloudinary\Contracts\UploadOptions as UploadOptionsContract;

class UploadOptions extends Fluent implements UploadOptionsContract
{
    public function toArray(): array
    {
        return parent::toArray();
    }

    public function setResourceType(string $resourceType): UploadOptionsContract
    {
        $this->offsetSet('resource_type', $resourceType);
        return $this;
    }

    public function setFolder(string $folder): UploadOptionsContract
    {
        $this->offsetSet('folder', $folder);
        return $this;
    }

    public function setPublicId(string $publicId): UploadOptionsContract
    {
        $this->offsetSet('public_id', $publicId);

        return $this;
    }
}
