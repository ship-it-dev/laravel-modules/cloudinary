<?php


namespace ShipIT\Cloudinary\Traits;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use ShipIT\Cloudinary\Models\File;

/**
 * @property-read Collection $cloudinaryFiles
 */
trait HasFiles
{
    public static function bootHasFiles(): void
    {
        self::deleted(function (Model $subject) {
            // TODO - delete file in cloudinary if model is deleted?
            // TODO - write test if works
            $subject
                ->cloudinaryFiles()
                ->each(function (File $file) {
                    $file->delete();
                });
        });
    }

    public function cloudinaryFiles(): MorphToMany
    {
        return $this->morphToMany(File::class, 'subject', 'cloudinary_file_subject');
    }
}