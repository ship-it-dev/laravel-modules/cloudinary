<?php


namespace ShipIT\Cloudinary\Contracts;


interface UploadOptions
{
    public const RESOURCE_TYPE_VIDEO = 'video';
    public const RESOURCE_TYPE_IMAGE = 'image';

    public function toArray(): array;

    public function setResourceType(string $resourceType): UploadOptions;

    public function setFolder(string $folder): UploadOptions;

    public function setPublicId(string $publicId): UploadOptions;
}
