<?php


namespace ShipIT\Cloudinary\Observers;


use Illuminate\Contracts\Bus\Dispatcher;
use ShipIT\Cloudinary\Jobs\DeleteResource;
use ShipIT\Cloudinary\Models\File;

class FileObserver
{
    private Dispatcher $jobDispatcher;

    public function __construct(
        Dispatcher $jobDispatcher
    )
    {
        $this->jobDispatcher = $jobDispatcher;
    }

    public function deleted(File $file): void
    {
        $this->jobDispatcher->dispatch(
            new DeleteResource($file->public_id)
        );
    }
}