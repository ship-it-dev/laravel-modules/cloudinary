<?php


namespace ShipIT\Cloudinary\Models;


use Illuminate\Support\Fluent;
use ShipIT\Cloudinary\Contracts\UploadResponse as UploadResponseContract;

class UploadResponse extends Fluent implements UploadResponseContract
{
    public function getSecureUrl(): string
    {
        return $this->get('secure_url');
    }

    public function toArray(): array
    {
        return parent::toArray();
    }
}
