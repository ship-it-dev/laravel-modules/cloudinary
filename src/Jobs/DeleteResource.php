<?php

namespace ShipIT\Cloudinary\Jobs;

use Cloudinary\Api;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteResource implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private string $publicId;

    public function __construct(
        string $publicId
    )
    {
        $this->publicId = $publicId;
    }

    public function handle(
        Api $api
    )
    {
        $api->delete_resources($this->publicId);
    }
}
