<?php


namespace ShipIT\Cloudinary\Models;


use Illuminate\Contracts\Container\Container;
use ShipIT\Cloudinary\Contracts\Uploader as UploaderContract;
use ShipIT\Cloudinary\Contracts\UploadOptions;
use ShipIT\Cloudinary\Contracts\UploadResponse;

class Uploader implements UploaderContract
{
    /**
     * @var Container
     */
    private Container $container;

    public function __construct(
        Container $container
    )
    {
        $this->container = $container;
    }

    public function upload(string $path, UploadOptions $options): UploadResponse
    {
        return $this->makeResponse(
            \Cloudinary\Uploader::upload($path, $options->toArray())
        );
    }

    private function makeResponse(array $attributes): UploadResponse
    {
        return $this->container->make(
            UploadResponse::class,
            [
                'attributes' => $attributes
            ]
        );
    }

    public function uploadLarge(string $path, UploadOptions $options): UploadResponse
    {
        return $this->makeResponse(
            \Cloudinary\Uploader::upload_large($path, $options->toArray())
        );
    }
}