<?php


namespace ShipIT\Cloudinary\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * @property string $public_id
 * @property string $resource_type
 */
class File extends Model
{
    public const TABLE_NAME           = 'cloudinary_files';
    public const COLUMN_PUBLIC_ID     = 'public_id';
    public const COLUMN_RESOURCE_TYPE = 'resource_type';

    protected $table = self::TABLE_NAME;

    protected $fillable = [
        self::COLUMN_PUBLIC_ID,
        self::COLUMN_RESOURCE_TYPE
    ];
}
